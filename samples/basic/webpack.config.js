const path = require('path');

module.exports = {
    entry: 'styleguide.js',
    resolve: {
        modules: [
            path.resolve(`${__dirname}/src`),
            path.resolve(`${__dirname}/node_modules`),
            path.resolve(`${__dirname}/node_modules/stylegud-ui/src/renderers`),
        ],
        alias: {
            vue: 'vue/dist/vue.esm.js',
        },
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            outputStyle: 'compressed'
                        }
                    }
                ]
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, 'server'),
    },
};
