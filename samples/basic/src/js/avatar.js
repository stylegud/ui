import Vue from 'vue';


const template = '<div class="vf-avatar" v-if="avatar.url" :class="getClasses" :style="getBackgroundImage"></div>';

/**
 * Base avatar
 *
 * @data {"avatar": { "url": "http://placebear.com/150/150" }, "size": "large"} - This is a base avatar
 *
 * @styleguide sample.components.avatar
 */
export const baseAvatarComponent = Vue.extend({
    template,
    props: [ 'avatar', 'size' ],
    computed: {
        getBackgroundImage() {
            if (this.avatar.url) {
                return `background-image: url("${this.avatar.url}")`;
            }
            return '';
        },
        getClasses() {
            return `vf-avatar-${this.size}`;
        }
    }
});
