import Vue from 'vue';


export const styleguideNav = Vue.extend({
    template: `
    <nav>
        <ul class="sg-nav">
            <li class="sg-nav-item" v-for="item in items" :key="item.referenceURI">
                <router-link :to="item.referenceURI" active-class="sg-nav-item-selected">{{item.header}}</router-link>
            </li>
        </ul>
    </nav>`,

    props: [ 'items' ]
});
