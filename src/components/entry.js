import Vue from 'vue';
import { styleguideNav } from './nav.js';
import { styleguideSection } from './section.js';


/**
 * styleguide
 *
 * Creates a styleguide, given a set of sections
 *
 * @exports styleguide
 * @constructor {Styleguide} Styleguide
 */
export const entry = Vue.extend({
    template: `
<section id="home">
    <header id="header">
        <h1>{{header}}</h1>
        <styleguide-nav :items="topLevelSections"></styleguide-nav>
    </header>
    <styleguide-nav v-if="isSubsection" name="subNav" class="sidebar" :items="subsections"></styleguide-nav>
    <styleguide-section name="sections" :item="currentSection" :recursive="isSubsection"></styleguide-section>
</section>`,

    props: [ 'header', 'initialSectionRef', 'currentSectionUri' ],

    computed: {
        isSubsection() {
            return this.currentSection.reference !== this.initialSection.reference;
        },
        currentSubSection() {
            return this.$sectionByRef(this.currentSection.reference.split('.').slice(0, 2).join('.') || {});
        },
        initialSection() {
            return this.$sectionByRef(this.initialSectionRef);
        },
        currentSection() {
            return this.$sectionByUri(this.currentSectionUri) || this.initialSection;
        },
        topLevelSection() {
            return this.$sectionByRef(this.initialSectionRef.split('.')[0]);
        },
        topLevelSections() {
            return this.$sectionsByRef(this.topLevelSection.sections);
        },
        subsections() {
            return this.$sectionsByRef(this.currentSubSection.sections);
        }
    },

    components: {
        styleguideNav,
        styleguideSection
    }
});
