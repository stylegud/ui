import Vue from 'vue';
import { renderedItem } from './renderedItem';


export const styleguideSection = Vue.extend({

    // This is needed to allow a circular reference
    name: 'styleguide-section',

    template: `
    <section :id="item.referenceURI" class="sg-primary-section">
        <h1>{{item.header}}</h1>
        <div class="section-description" v-if="item.description" v-html="item.description"></div>
        <rendered-item
            v-if="renderers.length > 0"
            v-for="renderer in renderers"
            :renderer="renderer"
            :key="renderer.id"
        ></rendered-item>
        <styleguide-section
            v-if="recursive"
            v-for="section in this.$sectionsByRef(item.sections)"
            :item="section"
            :key="section.referenceURI"
        ></styleguide-section>
    </section>`,

    props: {
        item: { type: Object },
        recursive: { type: Boolean, default: true }
    },

    computed: {
        renderers() {
            return this.item.getRenderers(this.item);
        }
    },

    components: {
        renderedItem
    }
});
