import Vue from 'vue';


export const codeSnippet = Vue.extend({
    template: `
        <div class="sg-overlay">
            Markup: <pre class="sg-sample"><slot>{{code}}</slot></pre>
            <template v-if="props">
                Props: <pre><slot>{{props}}</slot></pre>
            </template>
        </div>`,

    props: [ 'code', 'props' ]
});
