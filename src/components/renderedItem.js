import Vue from 'vue';
import { codeSnippet } from './codeSnippet';


const renderContainerClass = 'sg-render-container';
export const renderedItem = Vue.extend({
    template: `
        <div class="sg-component-instance">
            <h4 class="sg-component-title" v-if="renderer.getTitle()">{{renderer.getTitle()}}:</h4>
            <p class="sg-component-description" v-if="renderer.getDescription()">{{renderer.getDescription()}}</p>
            <div :class="classes"></div>
            <button @click="togglePreview" class="sg-show-snippet">Show Code</button>
            <code-snippet v-show="showPreview" @close="togglePreview" :code="renderer.getMarkup()" :props="snippetProps"></code-snippet>
        </div>`,

    props: [ 'renderer' ],

    data() {
        const classes = {
            'sg-render-container': true,
            // eslint-disable-next-line no-undef
            [window.__style_base_class__]: Boolean(window.__style_base_class__)
        };
        return {
            classes,
            showPreview: false
        };
    },

    mounted() {
        this.renderer.renderItem(this.$el.querySelector(`.${renderContainerClass}`));
    },

    methods: {
        togglePreview() {
            this.showPreview = !this.showPreview;
        }
    },

    computed: {
        snippetProps() {
            return this.renderer._data && this.renderer._data.datum;
        }
    },

    components: {
        codeSnippet
    }
});
