import Vue from 'vue';
import R from 'ramda';
import { BaseRenderer } from './renderer';

/**
 * Things
 *
 * @export
 * @param {Object} data Configuration for the renderer
 * @returns {VueRenderer[]} A set of vue renderers
 */
export function getRenderers(data) {
    const tagName = camelCaseToDash(data.source.export);
    const component = data.source.exportedComponent;
    Vue.component(tagName, component);
    const markup = data.markup.length > 0 ? data.markup : [ undefined ];
    const innerData = (data.source.data.length < 1 ? [ {} ] : data.source.data)
        .map((datum) => markup.map((markup) => [ datum, markup ]));
    return R.unnest(innerData)
        .map(([ datum, markup ]) => new VueRenderer(tagName, component, datum, markup));
}

/**
 * Converts camel case to kebab case
 *
 * @param {String} myStr The string to convert
 * @returns {String} The converted string
 */
function camelCaseToDash(myStr) {
    return myStr.replace(/([A-Z])/g, (g) => `-${g[0].toLowerCase()}`);
}

/**
 * A renderer for Vue components
 *
 * @param {String} tagName The tag to use when rendering the component
 * @param {Object} component The component itself (already imported)
 * @param {Object[]} data The data for the component to use when rendering
 * @param {String} markup The markup to use (overrides tagname generation)
 */
function VueRenderer(tagName, component, data, markup) {
    this._tagName = tagName;
    this._component = Vue.extend(component);
    this._data = data || {};
    this._baseMarkup = markup;
    this._markup = this._getMarkup();
}

VueRenderer.prototype = Object.create(BaseRenderer.prototype);

VueRenderer.prototype._renderItem = function(el) {
    el.innerHTML = this._markup;
    const components = {
        [this._tagName]: this._component
    };
    this.vue = new Vue({
        el,
        components,
        data: () => Object.assign({}, this._data, this._data.datum)
    });
};

VueRenderer.prototype._getMarkup = function() {
    const data = this._data.datum || this._data;
    const vueProps = Object.keys(this._component.props || this._component.options.props || {})
        .filter((klass) => Boolean(data[klass]))
        .map((p) => [ `:${camelCaseToDash(p)}`, p ]);
    const otherAttrs = Object.keys(data.attrs || {})
        .map((k) => [ k, data.attrs[k] ]);

    const attrs = [].concat(vueProps, otherAttrs);

    const slots = Object.keys(data.slots || {})
        .map((s) => [ s, data.slots[s] ]);

    if (this._baseMarkup) {
        return injectAttrs(this._baseMarkup, attrs);
    }
    return writeMarkup(this._tagName, attrs, slots, data.slot);
};

VueRenderer.prototype._getTitle = function() {
    return this._data.name;
};


VueRenderer.prototype._getDescription = function() {
    return this._data.description;
};

/**
 * Writes markup for components
 *
 * @param {String} tagName The tag for the component to use
 * @param {String[]} [attrs=[]] A list of properties to add to the component
 * @param {String[]} [slots=[]] A list of slots to pass into the component
 * @param {String[]} [slot=''] A slot to pass into the component
 * @returns {String} The compiled string markup for a component
 */
function writeMarkup(tagName, attrs = [], slots = [], slot = '') {
    attrs = attrs.map(([ p, v ]) => `${p}="${v}"`).join(' ');
    slots = slots.map(([ k, v ]) => `<template slot="${k}">${v}</template>`);
    return `<${tagName} ${attrs}>${slots}${slot}</${tagName}>`;
}

/**
 * Injects attributes into markup
 * @param {String} markup The base markup to use
 * @param {Object[]} attrs The properties to add to the markup
 * @returns {String} The markup with attrs injected
 */
function injectAttrs(markup, attrs = []) {
    if (attrs.length < 1) {
        return markup;
    }
    const el = getElFromString(markup);
    attrs.forEach(([ p, v ]) => el.setAttribute(p, v));
    return el.outerHTML;
}

/**
 * From StackOverflow
 *
 * @link https://stackoverflow.com/questions/494143/creating-a-new-dom-element-from-an-html-string-using-built-in-dom-methods-or-pro
 * @param {String} str The string from which to create a dom element
 * @return {Element} A DOM element from the string
 */
function getElFromString(str) {
    const el = document.createElement('div');
    el.innerHTML = str;
    return el.childNodes[0];
}
