let counter = 0;

/**
 * BaseRenderer
 *
 * A base interface for all renderers to implement, just throws
 * exceptions
 *
 * @export
 */
export function BaseRenderer() {}

/**
 * Define a unique id for each renderer for sorting in Vue
 */
Object.defineProperty(BaseRenderer.prototype, 'id', {
    get() {
        if (this._id) {
            return this._id;
        }
        // eslint-disable-next-line no-return-assign
        return this._id = counter++;
    }
});

/**
 * renderItem
 *
 * Calls this_renderItem with an HTMLElement, which should render an the
 * final version of a component into the HTMLElement
 *
 * @param {HTMLElement} el The element into which to insert the renderred component
 */
BaseRenderer.prototype.renderItem = function(el) {
    this._renderItem(el);
};

/**
 * getMarkup
 *
 * Returns an HTML escaped version of a component for showing the intended markup
 * to be used by developers
 *
 * @returns {String} The HTML escaped string of markup to use for rendering a component
 */
BaseRenderer.prototype.getMarkup = function() {
    return this._getMarkup();
};

/**
 * getTitle
 *
 * Returns a title string
 *
 * @returns {String} A title
 */
BaseRenderer.prototype.getTitle = function() {
    return this._getTitle();
};

/**
 * getDescription
 *
 * Returns a description string
 *
 * @returns {String} A description
 */
BaseRenderer.prototype.getDescription = function() {
    return this._getDescription();
};

/**
 * _renderItem
 *
 * To be overridden by concrete implementations. Should insert a rendered
 * component into el
 *
 * @param {HTMLElement} el The element to render
 */
BaseRenderer.prototype._renderItem = function(el) {
    throw Error('_renderItem must be implemented on all renderers', el);
};

/**
 * _getMarkup
 *
 * To be overridden by concrete implementations. Should return unescaped HTML
 * markup to be used by developers who wish to implement a component.
 */
BaseRenderer.prototype._getMarkup = function() {
    throw Error('_getMarkup must be implemented on all renderers');
};

/**
 * _getTitle
 *
 * To be overridden by concrete implementations. Should return unescaped string
 * title for this rendering of a component.
 */
BaseRenderer.prototype._getTitle = function() {
    throw Error('_getTitle must be implemented on all renderers');
};


/**
 * _getDescription
 *
 * To be overridden by concrete implementations. Should return unescaped string
 * description for this rendering of a component.
 */
BaseRenderer.prototype._getDescription = function() {
    throw Error('_getDescription must be implemented on all renderers');
};
