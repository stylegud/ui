import { BaseRenderer } from './renderer';


/**
 * Things
 *
 * @export
 * @param {Object} data Configuration for the renderer
 * @returns {VueRenderer[]} A set of vue renderers
 */
export function getRenderers(data) {
    const { markup, source } = data;
    if (!markup || markup.length < 1) {
        return [];
    }
    const mods = source.data.length > 0 ? source.data : [ undefined ];
    return mods.map((mod) => new CSSRenderer(markup[0], mod));
}

/**
 * CSSRenderer
 *
 * Renders KSS into markup for display in a styleguide
 *
 * @param {String} markup The html markup to render with params
 * @param {String} data The data to render into the html markup
 * @constructor {CSSRenderer} CSSRenderer
 */
function CSSRenderer(markup, data) {
    this._rendered = renderMarkup(markup, data);
    this._title = data && data.description;
}

CSSRenderer.prototype = Object.create(BaseRenderer.prototype);

CSSRenderer.prototype._renderItem = function(el) {
    el.innerHTML = this._rendered;
};

CSSRenderer.prototype._getMarkup = function() {
    return this._rendered;
};

CSSRenderer.prototype._getTitle = function() {
    return this._title;
};

CSSRenderer.prototype._getDescription = function() {
    return this._description || '';
};

/**
 * renderMarkup - Renders markup
 *
 * @param {String} [markup=''] The markup to render
 * @param {Object} [data={}] The params to use to template the markup
 * @returns {String} The templated markup
 */
function renderMarkup(markup = '', data = {}) {
    for (let key of Object.keys(data)) {
        markup = markup.replace(new RegExp(`\{\{${key}\}\}`, 'g'), data[key]);
    }
    // Special case for KSS compatibility
    markup = markup.replace(new RegExp('\{\{modifier_class\}\}', 'g'), data.className);
    return markup;
}
