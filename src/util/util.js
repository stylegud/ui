/**
 * Generates a key
 *
 * @return {String} The next key
 */
export function keyGenerator() {
    let key = 0;
    return function() {
        return `${key++}`;
    };
}

/**
 * Creates a key value store from an array of objects using one key from the internal objects
 *
 * @param {String} key A string key name to use for creating object keys
 * @param {Section[]} arr An array of sections
 * @returns {Object} The created object
 */
export const objectFromArrayByKey = (key, arr) => arr.reduce((acc, curr) => {
    const keyVal = curr[key];
    acc[keyVal] = curr;
    return acc;
}, {});
